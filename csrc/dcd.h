typedef struct dcd_file_t_ {
  int fd;
  char hdr[5];
  int N;
  int nset;
  int itstart;
  int tbsave;
  float timestep;
  int wcell;
  char title[80];
  char user[80];
  long blsize;
  long start_pos;
} dcd_file_t;

int scan_dcd(int fd, dcd_file_t *dcd_file, int flag);

int gdcdp(dcd_file_t *dcd_file, float *x, float *y, float *z, int n);

int sdcdp(dcd_file_t *dcd_file, float *x, float *y, float *z, int n);

int close_dcd(dcd_file_t *dcd_file);

int create_dcd(char *path, int N, int itstart, int tbsave,
               float timestep, int wcell);
