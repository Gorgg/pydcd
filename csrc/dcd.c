#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/uio.h>
#include <time.h>
#include "dcd.h"

/* #define TIMEFACTOR 48.88821 */
#define TIMEFACTOR 1.0

/* Scans the dcd file at the file descriptor given by fd and
 * initializes metadata in the dcd_file struct. flag may be used to
 * print certain values obtained. The original file descriptor is
 * duplicated, meaning that it can (and should) be closed by outside
 * code.
 * Returns:
 *   0 - success
 *  -1 - error
 */
int scan_dcd(int fd, dcd_file_t *dcd_file, int flag)
{
  int tempis[4];
  int headersize = 27*sizeof(int) + sizeof(float) + 164;

  dcd_file->fd = dup(fd);
  if(dcd_file->fd == -1)
    return -1;

  char *header = malloc(headersize);
  if (header == NULL){
    fprintf(stderr, "Error allocating memory for dcd header: %s\n", strerror(errno));
    close(dcd_file->fd);
    return -1;
  }

  lseek(dcd_file->fd, 0, SEEK_SET);
  if(read(dcd_file->fd, header, headersize) != headersize){
    fprintf(stderr, "Error reading dcd header: %s\n", strerror(errno));
    close(dcd_file->fd);
    free(header);
    return -1;
  }

  /* bsize */
  /* memcpy is needed to convert types portably, casting may not
   * work */
  memcpy(tempis, &header[0], sizeof(int));
  if(tempis[0] != 84){
    printf("May be a bad dcd format, but the code is not necessarily\n");
    printf("portable either.  The value of i should be 84, but it is %i\n", tempis[0]);
    close(dcd_file->fd);
    free(header);
    return -1;
  }

  memcpy(dcd_file->hdr, &header[sizeof(int)], 4);
  dcd_file->hdr[4] = '\0';

  memcpy(&dcd_file->nset, &header[sizeof(int) + 4], sizeof(int));
  memcpy(&dcd_file->itstart, &header[2*sizeof(int) + 4], sizeof(int));
  memcpy(&dcd_file->tbsave, &header[3*sizeof(int) + 4], sizeof(int));

  for(int i = 0; i < 5; i++){
    memcpy(tempis, &header[5*sizeof(int) + 4 + i], sizeof(int));
    if(tempis[0] != 0) fprintf(stderr, "Warning: Not created by NAMD\n");
  }

  memcpy(&dcd_file->timestep, &header[10*sizeof(int) + 4], sizeof(float));
  memcpy(&dcd_file->wcell, &header[10*sizeof(int) + sizeof(float) + 4], sizeof(int));
  if(dcd_file->wcell != 0) dcd_file->wcell = 1;
  dcd_file->timestep *= TIMEFACTOR;

  memcpy(tempis, &header[19*sizeof(int) + sizeof(float) + 4], 4 * sizeof(int));
  if(tempis[0] != 24 ||
     tempis[1] != 84 ||
     tempis[2] != 164 ||
     tempis[3] != 2){
    fprintf(stderr, "Error reading dcd header\n");
    close(dcd_file->fd);
    free(header);
    return -1;
  }

  memcpy(dcd_file->title, &header[23*sizeof(int) + sizeof(float) + 4], 80);
  dcd_file->title[79] = '\0';

  memcpy(dcd_file->user, &header[23*sizeof(int) + sizeof(float) + 84], 80);
  dcd_file->user[79] = '\0';

  memcpy(tempis, &header[23*sizeof(int) + sizeof(float) + 164], 2 * sizeof(int));
  if(tempis[0] != 164 ||
     tempis[1] != 4){
    fprintf(stderr, "Error reading dcd header\n");
    close(dcd_file->fd);
    free(header);
    return -1;
  }

  memcpy(&dcd_file->N, &header[25*sizeof(int) + sizeof(float) + 164], sizeof(int));

  memcpy(tempis, &header[26*sizeof(int) + sizeof(float) + 164], sizeof(int));
  if(tempis[0] != 4){
    fprintf(stderr, "Error reading dcd header\n");
    close(dcd_file->fd);
    free(header);
    return -1;
  }

  /* Save position of start of frame data */
  dcd_file->start_pos = headersize;

  if(flag){
    printf("Number of particles = %i\n", dcd_file->N);
    printf("Timestep = %f\n", dcd_file->timestep);
    printf("Number of steps between saves = %i\n", dcd_file->tbsave);
    printf("Starting timestep = %i\n", dcd_file->itstart);
    printf("Number of saved configurations = %i\n", dcd_file->nset);
  }

  free(header);

  dcd_file->blsize = 6*sizeof(int) + 3*dcd_file->N*sizeof(float) + dcd_file->wcell*(2*sizeof(int)+6*sizeof(double));
  //printf("%ld\n",&blsize);

  return 0;
}

/* Reads the nth frame from the file described by dcd_file. The
 * dcd_file struct must have been previously initialized by scan_dcd.
 * Returns:
 *   0 - success
 *  -1 - error
 */
int gdcdp(dcd_file_t *dcd_file, float *x, float *y, float *z, int n)
{
  int tempis[6];
  struct iovec iovecs[9];

  iovecs[0].iov_len =
  iovecs[2].iov_len =
  iovecs[3].iov_len =
  iovecs[5].iov_len =
  iovecs[6].iov_len =
  iovecs[8].iov_len = sizeof(int);

  iovecs[0].iov_base = &tempis[0];
  iovecs[2].iov_base = &tempis[1];
  iovecs[3].iov_base = &tempis[2];
  iovecs[5].iov_base = &tempis[3];
  iovecs[6].iov_base = &tempis[4];
  iovecs[8].iov_base = &tempis[5];

  iovecs[1].iov_len =
  iovecs[4].iov_len =
  iovecs[7].iov_len = dcd_file->N * sizeof(float);

  iovecs[1].iov_base = x;
  iovecs[4].iov_base = y;
  iovecs[7].iov_base = z;

  int read_size = 6 * sizeof(int) + 3 * dcd_file->N*sizeof(float);
  long offset = dcd_file->start_pos + n*dcd_file->blsize + dcd_file->wcell*(2*sizeof(int)+6*sizeof(double));
  if (preadv(dcd_file->fd, iovecs, 9, offset) != read_size){
    fprintf(stderr, "Error reading data from dcd file: %s\n", strerror(errno));
    return -1;
  }

  /* Check particle array size values */
  for(int i = 0; i < 6; i++){
    if(tempis[i] != dcd_file->N*sizeof(float)){
      fprintf(stderr, "Error: read dcd file data wrong\n");
      fprintf(stderr, "%i != %i\n", tempis[i], 4*dcd_file->N);
      return -1;
    }
  }

  return 0;
}

/* Sets the nth frame from the file described by dcd_file. The
 * dcd_file struct must have been previously initialized by scan_dcd.
 * Returns:
 *   0 - success
 *  -1 - error
 */
int sdcdp(dcd_file_t *dcd_file, float *x, float *y, float *z, int n)
{
  int write_size;
  long offset;
  struct iovec iovecs[9];

  if(dcd_file->wcell){
    int wcell_size_header = 6 * sizeof(double);
    double wcell_values[6];

    /* Assume our basis is composed of orthogonal unit vectors */
    wcell_values[0] =
    wcell_values[2] =
    wcell_values[5] = 1;

    wcell_values[1] =
    wcell_values[3] =
    wcell_values[4] = 0;

    iovecs[0].iov_len =
    iovecs[2].iov_len = sizeof(int);

    iovecs[0].iov_base =
    iovecs[2].iov_base = &wcell_size_header;

    iovecs[1].iov_len = 6 * sizeof(double);

    iovecs[1].iov_base = wcell_values;

    write_size = 2 * sizeof(int) + 6 * sizeof(double);
    offset = dcd_file->start_pos + n*dcd_file->blsize;
    if (pwritev(dcd_file->fd, iovecs, 3, offset) != write_size){
      fprintf(stderr, "Error writing wcell data to dcd file: %s\n", strerror(errno));
      return -1;
    }
  }

  iovecs[0].iov_len =
  iovecs[2].iov_len =
  iovecs[3].iov_len =
  iovecs[5].iov_len =
  iovecs[6].iov_len =
  iovecs[8].iov_len = sizeof(int);

  int n_particles_header = dcd_file->N * sizeof(float);

  iovecs[0].iov_base =
  iovecs[2].iov_base =
  iovecs[3].iov_base =
  iovecs[5].iov_base =
  iovecs[6].iov_base =
  iovecs[8].iov_base = &n_particles_header;

  iovecs[1].iov_len =
  iovecs[4].iov_len =
  iovecs[7].iov_len = dcd_file->N * sizeof(float);

  iovecs[1].iov_base = x;
  iovecs[4].iov_base = y;
  iovecs[7].iov_base = z;

  write_size = 6 * sizeof(int) + 3 * dcd_file->N*sizeof(float);
  offset = dcd_file->start_pos + n*dcd_file->blsize + dcd_file->wcell*(2*sizeof(int)+6*sizeof(double));
  if (pwritev(dcd_file->fd, iovecs, 9, offset) != write_size){
    fprintf(stderr, "Error writing data to dcd file: %s\n", strerror(errno));
    return -1;
  }

  /* Update number of frames in dcd file */
  if(n >= dcd_file->nset){
    dcd_file->nset = n + 1;
    if (pwrite(dcd_file->fd, &dcd_file->nset, sizeof(int), sizeof(int) + 4) != sizeof(int)){
      fprintf(stderr, "Error updating number of frames in dcd file: %s\n", strerror(errno));
      fprintf(stderr, "File will be left in inconsistent state\n");
      return -1;
    }

    int last_step = n * dcd_file->tbsave;
    if (pwrite(dcd_file->fd, &last_step, sizeof(int), 4*sizeof(int) + 4) != sizeof(int)){
      fprintf(stderr, "Error updating number of frames in dcd file: %s\n", strerror(errno));
      fprintf(stderr, "File will be left in inconsistent state\n");
      return -1;
    }
  }

  return 0;
}

/* Closes resources associated with the dcd_file struct. This must be
 * called before freeing a previously initialized dcd_file.
 * Returns:
 *   0 - success
 *  -1 - error
 */
int close_dcd(dcd_file_t *dcd_file){
  return((close(dcd_file->fd) == 0) ? 0 : -1);
}

/* Creates a new dcd file with header.
 * Returns:
 *   fd - success
 *  -1  - error
 */
int create_dcd(char *path, int N, int itstart, int tbsave,
               float timestep, int wcell)
{
  int headersize = 27*sizeof(int) + sizeof(float) + 164;
  int tempis[4];

  int fd = open(path, O_RDWR | O_CREAT | O_EXCL, 0666);
  if(fd < 0){
    fprintf(stderr, "Error creating dcd file: %s\n", strerror(errno));
    return -1;
  }

  char *header = calloc(1, headersize);
  if (header == NULL){
    fprintf(stderr, "Error allocating memory for dcd header: %s\n", strerror(errno));
    close(fd);
    return -1;
  }

  tempis[0] = 84;
  memcpy(header, tempis, sizeof(int));

  strncpy(&header[sizeof(int)], "CORD", 4);

  memcpy(&header[2*sizeof(int) + 4], &itstart, sizeof(int));
  memcpy(&header[3*sizeof(int) + 4], &tbsave, sizeof(int));
  memcpy(&header[10*sizeof(int) + 4], &timestep, sizeof(float));
  memcpy(&header[10*sizeof(int) + sizeof(float) + 4], &wcell, sizeof(int));

  tempis[0] = 24;
  tempis[1] = 84;
  tempis[2] = 164;
  tempis[3] = 2;
  memcpy(&header[19*sizeof(int) + sizeof(float) + 4], tempis, 4 * sizeof(int));

  strncpy(&header[23*sizeof(int) + sizeof(float) + 4], "Created by pydcd", 17);
  time_t cur_time = time(NULL);
  struct tm *tmbuf = localtime(&cur_time);
  strftime(&header[23*sizeof(int) + sizeof(float) + 84], 80, "REMARKS Created %d %B, %Y at %H:%M", tmbuf);
  header[23*sizeof(int) + sizeof(float) + 163] = '\0';

  tempis[0] = 164;
  tempis[1] = 4;
  memcpy(&header[23*sizeof(int) + sizeof(float) + 164], tempis, 2 * sizeof(int));

  memcpy(&header[25*sizeof(int) + sizeof(float) + 164], &N, sizeof(int));

  tempis[0] = 4;
  memcpy(&header[26*sizeof(int) + sizeof(float) + 164], tempis, sizeof(int));

  if(write(fd, header, headersize) != headersize){
    fprintf(stderr, "Error writing dcd header: %s\n", strerror(errno));
    free(header);
    close(fd);
    return -1;
  }

  free(header);

  return fd;
}
