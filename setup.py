from setuptools import find_packages, setup

setup(
  name="pydcd",
  version="0.0.4",
  packages=find_packages(include=["pydcd"]),
  description="Small accelerated utility to read dcd trajectory files",
  setup_requires=["cffi>=1.12.0"],
  cffi_modules=["csrc/pydcd_extension_build.py:ffibuilder"],
  install_requires=["cffi>=1.12.0", "numpy>=1.0.0"],
)
