import numpy
import os
from _pydcd import ffi, lib

class dcdfile:
  """
  A class that tracks and is used to inteface with an open dcd file.
  The class makes a copy of the open fd which refers to the file, and
  the copied fd is close upon object destruction.
  Attributes that can be accessed include:
    - N - number of particles
    - nset - number of frames
    - itstart - start time
    - tbsave - number of steps between saves
    - timestep - time difference between frames
    - wcell
  """
  def __init__(self, file):
    self.dcd_file = ffi.new("dcd_file_t *")

    if file.readable() != True:
      raise TypeError("File given is not readable")

    try:
      fd = file.fileno()
    except OSError:
      raise TypeError("Could not get fd (fileno()) from given file object")

    if lib.scan_dcd(fd, self.dcd_file, 0) != 0:
      raise RuntimeError("scan_dcd exited with error status")

    self.N = self.dcd_file[0].N
    self.nset = self.dcd_file[0].nset
    self.itstart = self.dcd_file[0].itstart
    self.tbsave = self.dcd_file[0].tbsave
    self.timestep = self.dcd_file[0].timestep
    self.wcell = self.dcd_file[0].wcell

  def check_arrays(self, x_array, y_array, z_array):
    for array in x_array, y_array, z_array:
      if array.data.c_contiguous != True:
        raise TypeError("Data underlying numpy arrays must be accessed in c_contiguous order to interface with C functions correctly")

      if array.dtype != numpy.single:
        raise TypeError("The underlying data type of the numpy arrays must be equivalent to the C float type (numpy.single)")

      if array.size != self.N:
        raise TypeError("The size of each of the numpy arrays must be equal to the number of particles in the dcd file")

  def gdcdp(self, x_array, y_array, z_array, pos):
    """
    Get the set of x, y and z particle coordinates in the dcd file at
    frame number pos. numpy arrays for each dimension of coordinates
    are passed in and filled with data.
    """
    if pos < 0 or pos >= self.nset:
      raise IndexError("Frame number to get out of range, must be pre-existing frame in file")

    self.check_arrays(x_array, y_array, z_array)

    if lib.gdcdp(self.dcd_file, ffi.from_buffer("float[]", x_array),
                                ffi.from_buffer("float[]", y_array),
                                ffi.from_buffer("float[]", z_array), pos) != 0:
      raise RuntimeError("gdcdp exited with error status")

  def sdcdp(self, x_array, y_array, z_array, pos):
    """
    Set the x, y, and z particle coordinates in the dcd file for
    pre-existing frame number pos. numpy arrays for each dimension of
    coordinates are used.
    """
    if pos < 0 or pos >= self.nset:
      raise IndexError("Frame number to set out of range, must be pre-existing frame in file")

    self.check_arrays(x_array, y_array, z_array)

    if lib.sdcdp(self.dcd_file, ffi.from_buffer("float[]", x_array),
                                ffi.from_buffer("float[]", y_array),
                                ffi.from_buffer("float[]", z_array), pos) != 0:
      raise RuntimeError("sdcdp exited with error status")

  def adcdp(self, x_array, y_array, z_array):
    """
    Append a new frame to the end of the dcd file with set of x, y, and
    z particle coordinates passed in as numpy arrays. numpy arrays for
    each dimension of coordinates are used.
    """
    self.check_arrays(x_array, y_array, z_array)

    if lib.sdcdp(self.dcd_file, ffi.from_buffer("float[]", x_array),
                                ffi.from_buffer("float[]", y_array),
                                ffi.from_buffer("float[]", z_array), self.nset) != 0:
      raise RuntimeError("sdcdp exited with error status")

    # Update number of frames in file
    self.nset = self.dcd_file[0].nset

  def __del__(self):
    if lib.close_dcd(self.dcd_file) != 0:
      raise RuntimeError("Error closing dcd file")

def create(path, N, itstart, tbsave, timestep, wcell):
  """
  Create new dcd file with header and corresponding dcdfile object.
  Desired properties of the file and its data are passed as arguments.
  """
  fd = lib.create_dcd(path.encode("ascii"), N, itstart, tbsave, timestep, wcell)
  if fd < 0:
    raise RuntimeError("create_dcd exited with error status")

  file = os.fdopen(fd)
  dcd_file = dcdfile(file)
  del file

  return dcd_file
